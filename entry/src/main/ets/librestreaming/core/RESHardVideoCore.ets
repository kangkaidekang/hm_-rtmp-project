import { RESVideoCore } from "./RESVideoCore";
import { RESCoreParameters } from "./RESCoreParameters";
import { BaseHardVideoFilter } from "./BaseHardVideoFilter";
import { RESScreenShotListener } from "./RESScreenShotListener";
import { RESVideoChangeListener } from "./RESVideoChangeListener";
import { HandlerThread } from "./HandlerThread";
import { VideoGLHandler } from "./VideoGLHandler";
import { Lock } from "./Lock";
import { MediaCodec } from "./MediaCodec";
import { MediaFormat } from "./MediaFormat";
import { ReentrantLock } from "./ReentrantLock";

export class RESHardVideoCore implements RESVideoCore {
  private resCoreParameters: RESCoreParameters;
  private syncOp: Object = {};
  private lockVideoFilter: Lock | null = null;
  private videoFilter: BaseHardVideoFilter | undefined;
  private dstVideoEncoder: MediaCodec | undefined;
  private dstVideoFormat: MediaFormat | undefined;
  private syncPreview: Object = {};
  private videoGLHandlerThread: HandlerThread | undefined;
  private videoGLHander: VideoGLHandler | undefined;

  private syncResScreenShotListener: Object = {};
  private resScreenShotListener: RESScreenShotListener | undefined;
  private syncResVideoChangeListener: Object = {};
  private resVideoChangeListener: RESVideoChangeListener | undefined;
  private syncIsLooping: Object = {};
  private isPreviewing: boolean = false;
  private isStreaming: boolean = false;
  private loopingInterval: number = 0;

  private isEnableMirror: boolean = false;
  private isEnablePreviewMirror: boolean = false;
  private isEnableStreamMirror: boolean = false;

  constructor(parameters: RESCoreParameters) {
    this.resCoreParameters = parameters;
    this.lockVideoFilter = new ReentrantLock(false);
  }

  public onFrameAvailable(): void {
    if (this.videoGLHandlerThread !== undefined) {
      if (this.videoGLHander !== undefined) {
        this.videoGLHander.addFrameNum();
      }
    }
  }

  public prepare(resConfig: RESConfig): boolean {
    synchronized(this.syncOp) {
      this.resCoreParameters.renderingMode = resConfig.getRenderingMode();
      this.resCoreParameters.mediacdoecAVCBitRate = resConfig.getBitRate();
      this.resCoreParameters.videoBufferQueueNum = resConfig.getVideoBufferQueueNum();
      this.resCoreParameters.mediacodecAVCIFrameInterval = resConfig.getVideoGOP();
      this.resCoreParameters.mediacodecAVCFrameRate = this.resCoreParameters.videoFPS;
      this.loopingInterval = 1000 / this.resCoreParameters.videoFPS;
      this.dstVideoFormat = new MediaFormat();
      this.videoGLHandlerThread = new HandlerThread("GLThread");
      this.videoGLHandlerThread.start();
      this.videoGLHander = new VideoGLHandler(this.videoGLHandlerThread.getLooper());
      this.videoGLHander.sendEmptyMessage(VideoGLHandler.WHAT_INIT);
      return true;
    }
  }

  public startPreview(surfaceTexture: SurfaceTexture, visualWidth: number, visualHeight: number): void {
    synchronized(this.syncOp) {
      this.videoGLHander.sendMessage(this.videoGLHander.obtainMessage(VideoGLHandler.WHAT_START_PREVIEW, visualWidth, visualHeight, surfaceTexture));
      synchronized(this.syncIsLooping) {
        if (!this.isPreviewing && !this.isStreaming) {
          this.videoGLHander.removeMessages(VideoGLHandler.WHAT_DRAW);
          this.videoGLHander.sendMessageDelayed(this.videoGLHander.obtainMessage(VideoGLHandler.WHAT_DRAW, SystemClock.uptimeMillis() + this.loopingInterval), this.loopingInterval);
        }
        this.isPreviewing = true;
      }
    }
  }

  public updatePreview(visualWidth: number, visualHeight: number): void {
    synchronized(this.syncOp) {
      synchronized(this.syncPreview) {
        this.videoGLHander.updatePreview(visualWidth, visualHeight);
      }
    }
  }

  public stopPreview(releaseTexture: boolean): void {
    synchronized(this.syncOp) {
      this.videoGLHander.sendMessage(this.videoGLHander.obtainMessage(VideoGLHandler.WHAT_STOP_PREVIEW, releaseTexture));
      synchronized(this.syncIsLooping) {
        this.isPreviewing = false;
      }
    }
  }

  public startStreaming(flvDataCollecter: RESFlvDataCollecter): boolean {
    synchronized(this.syncOp) {
      this.videoGLHander.sendMessage(this.videoGLHander.obtainMessage(VideoGLHandler.WHAT_START_STREAMING, flvDataCollecter));
      synchronized(this.syncIsLooping) {
        if (!this.isPreviewing && !this.isStreaming) {
          this.videoGLHander.removeMessages(VideoGLHandler.WHAT_DRAW);
          this.videoGLHander.sendMessageDelayed(this.videoGLHander.obtainMessage(VideoGLHandler.WHAT_DRAW, SystemClock.uptimeMillis() + this.loopingInterval), this.loopingInterval);
        }
        this.isStreaming = true;
      }
    }
    return true;
  }
  public updateCamTexture(camTex: SurfaceTexture): void {
    synchronized(this.syncOp) {
      if (this.videoGLHander !== undefined) {
        this.videoGLHander.updateCamTexture(camTex);
      }
    }
  }

  public stopStreaming(): boolean {
    synchronized(this.syncOp) {
      if (this.videoGLHander !== undefined) {
        this.videoGLHander.sendEmptyMessage(VideoGLHandler.WHAT_STOP_STREAMING);
      }
      synchronized(this.syncIsLooping) {
        this.isStreaming = false;
      }
    }
    return true;
  }

  @SuppressLint("NewApi")
  public destroy(): boolean {
    synchronized(this.syncOp) {
      if (this.videoGLHander !== undefined) {
        this.videoGLHander.sendEmptyMessage(VideoGLHandler.WHAT_UNINIT);
      }
      if (this.videoGLHandlerThread !== undefined) {
        this.videoGLHandlerThread.quitSafely();
        try {
          this.videoGLHandlerThread.join();
        } catch (ignored) {
        }
        this.videoGLHandlerThread = null;
      }
      this.videoGLHander = null;
      return true;
    }
  }

  @TargetApi(Build.VERSION_CODES.KITKAT)
  public reSetVideoBitrate(bitrate: number): void {
    synchronized(this.syncOp) {
      if (this.videoGLHander !== undefined) {
        this.videoGLHander.sendMessage(this.videoGLHander.obtainMessage(VideoGLHandler.WHAT_RESET_BITRATE, bitrate, 0));
        this.resCoreParameters.mediacdoecAVCBitRate = bitrate;
        if (this.dstVideoFormat !== undefined) {
          this.dstVideoFormat.setInteger(MediaFormat.KEY_BIT_RATE, this.resCoreParameters.mediacdoecAVCBitRate);
        }
      }
    }
  }

  @TargetApi(Build.VERSION_CODES.KITKAT)
  public getVideoBitrate(): number {
    synchronized(this.syncOp) {
      return this.resCoreParameters.mediacdoecAVCBitRate;
    }
  }

  public reSetVideoFPS(fps: number): void {
    synchronized(this.syncOp) {
      this.resCoreParameters.videoFPS = fps;
      this.loopingInterval = 1000 / this.resCoreParameters.videoFPS;
    }
  }

  public reSetVideoSize(newParameters: RESCoreParameters): void {
    synchronized(this.syncOp) {
      synchronized(this.syncIsLooping) {
        if (this.isPreviewing || this.isStreaming) {
          if (this.videoGLHander !== undefined) {
            this.videoGLHander.sendMessage(this.videoGLHander.obtainMessage(VideoGLHandler.WHAT_RESET_VIDEO, newParameters));
          }
        }
      }
    }
  }

  public setCurrentCamera(cameraIndex: number): void {
    this.mCameraId = cameraIndex;
    synchronized(this.syncOp) {
      if (this.videoGLHander !== undefined) {
        this.videoGLHander.updateCameraIndex(cameraIndex);
      }
    }
  }

  public acquireVideoFilter(): BaseHardVideoFilter | undefined {
    this.lockVideoFilter.lock();
    return this.videoFilter;
  }

  public releaseVideoFilter(): void {
    this.lockVideoFilter.unlock();
  }

  public setVideoFilter(baseHardVideoFilter: BaseHardVideoFilter): void {
    this.lockVideoFilter.lock();
    this.videoFilter = baseHardVideoFilter;
    this.lockVideoFilter.unlock();
  }

  public takeScreenShot(listener: RESScreenShotListener): void {
    synchronized(this.syncResScreenShotListener) {
      this.resScreenShotListener = listener;
    }
  }

  public setMirror(isEnableMirror: boolean, isEnablePreviewMirror: boolean, isEnableStreamMirror: boolean): void {
    this.isEnableMirror = isEnableMirror;
    this.isEnablePreviewMirror = isEnablePreviewMirror;
    this.isEnableStreamMirror = isEnableStreamMirror;
  }

  public setVideoChangeListener(listener: RESVideoChangeListener): void {
    synchronized(this.syncResVideoChangeListener) {
      this.resVideoChangeListener = listener;
    }
  }

  public getDrawFrameRate(): number {
    synchronized(this.syncOp) {
      return this.videoGLHander == undefined ? 0 : this.videoGLHander.getDrawFrameRate();
    }
  }

  class VideoGLHandler extends Handler {
  static readonly WHAT_INIT: number = 0x001;
  static readonly WHAT_UNINIT: number = 0x002;
  static readonly WHAT_FRAME: number = 0x003;
  static readonly WHAT_DRAW: number = 0x004;
  static readonly WHAT_RESET_VIDEO: number = 0x005;
  static readonly WHAT_START_PREVIEW: number = 0x010;
  static readonly WHAT_STOP_PREVIEW: number = 0x020;
  static readonly WHAT_START_STREAMING: number = 0x100;
  static readonly WHAT_STOP_STREAMING: number = 0x200;
  static readonly WHAT_RESET_BITRATE: number = 0x300;
  private screenSize: Size;
  private static readonly FILTER_LOCK_TOLERATION: number = 3; // 3ms
  private syncFrameNum: Object = {};
  private frameNum: number = 0;
  private syncCameraTex: Object = {};
  private cameraTexture: SurfaceTexture | undefined;
  private screenTexture: SurfaceTexture | undefined;
  private mediaCodecGLWapper: MediaCodecGLWapper | null = null;
  private screenGLWapper: ScreenGLWapper | null = null;
  private offScreenGLWapper: OffScreenGLWapper | null = null;
  private sample2DFrameBuffer: number = 0;
  private sample2DFrameBufferTexture: number = 0;
  private frameBuffer: number = 0;
  private frameBufferTexture: number = 0;
  private shapeVerticesBuffer: Float32Array | null = null;
  private mediaCodecTextureVerticesBuffer: Float32Array | null = null;
  private screenTextureVerticesBuffer: Float32Array | null = null;
  private currCamera: number = 0;
  private syncCameraTextureVerticesBuffer: Object = {};
  private camera2dTextureVerticesBuffer: Float32Array | null = null;
  private cameraTextureVerticesBuffer: Float32Array | null = null;
  private drawIndecesBuffer: Int16Array | null = null;
  private innerVideoFilter: BaseHardVideoFilter | null = null;
  private drawFrameRateMeter: RESFrameRateMeter;
  private directionFlag: number = 0;
  private videoSenderThread: VideoSenderThread | undefined;
  public hasNewFrame: boolean = false;
  public dropNextFrame: boolean = false;
  public textureMatrix: number[] | undefined;

  constructor(looper: Looper) {
    super(looper);
    this.screenGLWapper = null;
    this.mediaCodecGLWapper = null;
    this.drawFrameRateMeter = new RESFrameRateMeter();
    this.screenSize = new Size(1, 1);
    this.initBuffer();
  }

  handleMessage(msg: Message): void {
    switch (msg.what) {
      case VideoGLHandler.WHAT_FRAME: {
        GLHelper.makeCurrent(this.offScreenGLWapper);
        synchronized(this.syncFrameNum) {
          synchronized(this.syncCameraTex) {
            if (this.cameraTexture !== undefined) {
              while (this.frameNum !== 0) {
                this.cameraTexture.updateTexImage();
                this.frameNum--;
                if (!this.dropNextFrame) {
                  this.hasNewFrame = true;
                } else {
                  this.dropNextFrame = false;
                  this.hasNewFrame = false;
                }
              }
            } else {
              break;
            }
          }
        }
        this.drawSample2DFrameBuffer(this.cameraTexture);
        break;
      }
      case VideoGLHandler.WHAT_DRAW: {
        const time: number = msg.obj as number;
        let interval: number = time + loopingInterval - SystemClock.uptimeMillis();
        synchronized(this.syncIsLooping) {
          if (this.isPreviewing || this.isStreaming) {
            if (interval > 0) {
              this.videoGLHander.sendMessageDelayed(this.videoGLHander.obtainMessage(
                VideoGLHandler.WHAT_DRAW,
                SystemClock.uptimeMillis() + interval),
                interval);
            } else {
              this.videoGLHander.sendMessage(this.videoGLHander.obtainMessage(
                VideoGLHandler.WHAT_DRAW,
                SystemClock.uptimeMillis() + loopingInterval));
            }
          }
        }
        if (this.hasNewFrame) {
          this.drawFrameBuffer();
          this.drawMediaCodec(time * 1000000);
          this.drawScreen();
          this.encoderMp4(this.frameBufferTexture); // 编码MP4
          this.drawFrameRateMeter.count();
          this.hasNewFrame = false;
        }
        break;
      }
      case VideoGLHandler.WHAT_INIT: {
        this.initOffScreenGL();
        break;
      }
      case VideoGLHandler.WHAT_UNINIT: {
        lockVideoFilter.lock();
        if (this.innerVideoFilter !== null) {
          this.innerVideoFilter.onDestroy();
          this.innerVideoFilter = null;
        }
        lockVideoFilter.unlock();
        this.uninitOffScreenGL();
        break;
      }
      case VideoGLHandler.WHAT_START_PREVIEW: {
        this.initScreenGL(msg.obj as SurfaceTexture);
        this.updatePreview(msg.arg1, msg.arg2);
        break;
      }
      case VideoGLHandler.WHAT_STOP_PREVIEW: {
        this.uninitScreenGL();
        const releaseTexture: boolean = msg.obj as boolean;
        if (releaseTexture) {
          this.screenTexture!.release();
          this.screenTexture = null;
        }
        break;
      }
      case VideoGLHandler.WHAT_START_STREAMING: {
        if (this.dstVideoEncoder === null) {
          this.dstVideoEncoder = MediaCodecHelper.createHardVideoMediaCodec(this.resCoreParameters, this.dstVideoFormat);
          if (this.dstVideoEncoder === null) {
            throw new Error("create Video MediaCodec failed");
          }
        }
        this.dstVideoEncoder.configure(this.dstVideoFormat, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
        this.initMediaCodecGL(this.dstVideoEncoder.createInputSurface());
        this.dstVideoEncoder.start();
        this.videoSenderThread = new VideoSenderThread("VideoSenderThread", this.dstVideoEncoder, msg.obj as RESFlvDataCollecter);
        this.videoSenderThread.start();
        break;
      }
      case VideoGLHandler.WHAT_STOP_STREAMING: {
        this.videoSenderThread!.quit();
        try {
          this.videoSenderThread!.join();
        } catch (e) {
          LogTools.trace("RESHardVideoCore,stopStreaming()failed", e);
        }
        this.videoSenderThread = null;
        this.uninitMediaCodecGL();
        this.dstVideoEncoder!.stop();
        this.dstVideoEncoder!.release();
        this.dstVideoEncoder = null;
        break;
      }
      case VideoGLHandler.WHAT_RESET_BITRATE: {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && this.mediaCodecGLWapper !== null) {
          const bitrateBundle: Bundle = new Bundle();
          bitrateBundle.putInt(MediaCodec.PARAMETER_KEY_VIDEO_BITRATE, msg.arg1);
          this.dstVideoEncoder!.setParameters(bitrateBundle);
        }
        break;
      }
      case VideoGLHandler.WHAT_RESET_VIDEO: {
        const newParameters: RESCoreParameters = msg.obj as RESCoreParameters;
        this.resCoreParameters.videoWidth = newParameters.videoWidth;
        this.resCoreParameters.videoHeight = newParameters.videoHeight;
        this.resCoreParameters.cropRatio = newParameters.cropRatio;
        this.updateCameraIndex(this.currCamera);
        this.resetFrameBuff();
        if (this.mediaCodecGLWapper !== null) {
          this.uninitMediaCodecGL();
          this.dstVideoEncoder!.stop();
          this.dstVideoEncoder!.release();
          this.dstVideoEncoder = MediaCodecHelper.createHardVideoMediaCodec(this.resCoreParameters, this.dstVideoFormat);
          if (this.dstVideoEncoder === null) {
            throw new Error("create Video MediaCodec failed");
          }
          this.dstVideoEncoder.configure(this.dstVideoFormat, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
          this.initMediaCodecGL(this.dstVideoEncoder.createInputSurface());
          this.dstVideoEncoder.start();
          this.videoSenderThread!.updateMediaCodec(this.dstVideoEncoder);
        }
        synchronized (syncResVideoChangeListener) {
          if(resVideoChangeListener!==null) {
            CallbackDelivery.i().post(new RESVideoChangeListener.RESVideoChangeRunable(resVideoChangeListener,
              resCoreParameters.videoWidth,
              resCoreParameters.videoHeight));
          }
        }
        break;
      }
      default:
        break;
    }
  }

  private drawSample2DFrameBuffer(cameraTexture: SurfaceTexture): void {
    if (isEnableMirror) {
      screenTextureVerticesBuffer = GLHelper.adjustTextureFlip(isEnablePreviewMirror);
      mediaCodecTextureVerticesBuffer = GLHelper.adjustTextureFlip(isEnableStreamMirror);
    }

    GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, sample2DFrameBuffer);
    GLES20.glUseProgram(offScreenGLWapper.cam2dProgram);
    GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
    GLES20.glBindTexture(GLES20.GL_TEXTURE_EXTERNAL_OES, OVERWATCH_TEXTURE_ID);
    GLES20.glUniform1i(offScreenGLWapper.cam2dTextureLoc, 0);
    synchronized(syncCameraTextureVerticesBuffer) {
      GLHelper.enableVertex(offScreenGLWapper.cam2dPostionLoc, offScreenGLWapper.cam2dTextureCoordLoc,
        shapeVerticesBuffer, camera2dTextureVerticesBuffer);
    }
    textureMatrix = new Array(16).fill(0);
    cameraTexture.getTransformMatrix(textureMatrix);
    // encoder mp4 start
    // processStMatrix(textureMatrix, mCameraID == Camera.CameraInfo.CAMERA_FACING_FRONT);
    // encoder mp4 end
    GLES20.glUniformMatrix4fv(offScreenGLWapper.cam2dTextureMatrix, 1, false, textureMatrix, 0);
    GLES20.glViewport(0, 0, resCoreParameters.previewVideoHeight, resCoreParameters.previewVideoWidth); // resCoreParameters.videoWidth, resCoreParameters.videoHeight
    doGLDraw();
    GLES20.glFinish();
    GLHelper.disableVertex(offScreenGLWapper.cam2dPostionLoc, offScreenGLWapper.cam2dTextureCoordLoc);
    GLES20.glBindTexture(GLES20.GL_TEXTURE_EXTERNAL_OES, 0);
    GLES20.glUseProgram(0);
    GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
  }

  private drawOriginFrameBuffer(): void {
    GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, frameBuffer);
    GLES20.glUseProgram(offScreenGLWapper.camProgram);
    GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
    GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, sample2DFrameBufferTexture);
    GLES20.glUniform1i(offScreenGLWapper.camTextureLoc, 0);
    synchronized(syncCameraTextureVerticesBuffer) {
      GLHelper.enableVertex(offScreenGLWapper.camPostionLoc, offScreenGLWapper.camTextureCoordLoc,
        shapeVerticesBuffer, cameraTextureVerticesBuffer);
    }
    GLES20.glViewport(0, 0, resCoreParameters.previewVideoHeight, resCoreParameters.previewVideoWidth);
    doGLDraw();
    GLES20.glFinish();
    GLHelper.disableVertex(offScreenGLWapper.camPostionLoc, offScreenGLWapper.camTextureCoordLoc);
    GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
    GLES20.glUseProgram(0);
    GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
  }

  private drawFrameBuffer(): void {
    GLHelper.makeCurrent(offScreenGLWapper);
    const isFilterLocked: boolean = lockVideoFilter();
    const starttime: number = Date.now();
    if (isFilterLocked) {
      if (videoFilter !== innerVideoFilter) {
        if (innerVideoFilter !== null) {
          innerVideoFilter.onDestroy();
        }
        innerVideoFilter = videoFilter;
        if (innerVideoFilter !== null) {
          innerVideoFilter.onInit(resCoreParameters.previewVideoHeight, resCoreParameters.previewVideoWidth); // resCoreParameters.videoWidth, resCoreParameters.videoHeight
        }
      }
      if (innerVideoFilter !== null) {
        synchronized(syncCameraTextureVerticesBuffer) {
          innerVideoFilter.onDirectionUpdate(directionFlag);
          innerVideoFilter.onDraw(sample2DFrameBufferTexture, frameBuffer, shapeVerticesBuffer, cameraTextureVerticesBuffer);
        }
      } else {
        drawOriginFrameBuffer();
      }
      unlockVideoFilter();
    } else {
      drawOriginFrameBuffer();
    }
    LogTools.e("滤镜耗时：" + (Date.now() - starttime));
    GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, frameBuffer);
    checkScreenShot();
    GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
  }

  private drawMediaCodec(currTime: number): void {
    if (mediaCodecGLWapper !== null) {
      GLHelper.makeCurrent(mediaCodecGLWapper);
      GLES20.glUseProgram(mediaCodecGLWapper.drawProgram);
      GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
      GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, frameBufferTexture);
      GLES20.glUniform1i(mediaCodecGLWapper.drawTextureLoc, 0);
      GLHelper.enableVertex(mediaCodecGLWapper.drawPostionLoc, mediaCodecGLWapper.drawTextureCoordLoc,
        shapeVerticesBuffer, mediaCodecTextureVerticesBuffer);
      this.doGLDraw();
      GLES20.glFinish();
      GLHelper.disableVertex(mediaCodecGLWapper.drawPostionLoc, mediaCodecGLWapper.drawTextureCoordLoc);
      GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
      GLES20.glUseProgram(0);
      EGLExt.eglPresentationTimeANDROID(mediaCodecGLWapper.eglDisplay, mediaCodecGLWapper.eglSurface, currTime);
      if (!EGL14.eglSwapBuffers(mediaCodecGLWapper.eglDisplay, mediaCodecGLWapper.eglSurface)) {
        throw new Error("eglSwapBuffers,failed!");
      }
    }
  }

  private drawScreen(): void {
    if (screenGLWapper !== null) {
      GLHelper.makeCurrent(screenGLWapper);
      GLES20.glUseProgram(screenGLWapper.drawProgram);
      GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
      GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, frameBufferTexture);
      GLES20.glUniform1i(screenGLWapper.drawTextureLoc, 0);
      GLHelper.enableVertex(screenGLWapper.drawPostionLoc, screenGLWapper.drawTextureCoordLoc,
        shapeVerticesBuffer, screenTextureVerticesBuffer);
      GLES20.glViewport(0, 0, screenSize.getWidth(), screenSize.getHeight());
      this.doGLDraw();
      GLES20.glFinish();
      GLHelper.disableVertex(screenGLWapper.drawPostionLoc, screenGLWapper.drawTextureCoordLoc);
      GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
      GLES20.glUseProgram(0);
      if (!EGL14.eglSwapBuffers(screenGLWapper.eglDisplay, screenGLWapper.eglSurface)) {
        throw new Error("eglSwapBuffers,failed!");
      }
    }
  }

  private doGLDraw(): void {
    GLES20.glClearColor(0.0, 0.0, 0.0, 0.0);
    GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
    GLES20.glDrawElements(GLES20.GL_TRIANGLES, drawIndecesBuffer.limit(), GLES20.GL_UNSIGNED_SHORT, drawIndecesBuffer);
  }

  /**
   * @return true if filter locked & filter!=null
   */
  private lockVideoFilter(): boolean {
    try {
      return lockVideoFilter.tryLock(FILTER_LOCK_TOLERATION, TimeUnit.MILLISECONDS);
    } catch (e) {
      return false;
    }
  }

  private unlockVideoFilter(): void {
    lockVideoFilter.unlock();
  }

  private void checkScreenShot() {
    synchronized (syncResScreenShotListener) {
        if (resScreenShotListener != null) {
            Bitmap result = null;
            try {
                IntBuffer pixBuffer = IntBuffer.allocate(resCoreParameters.previewVideoHeight * resCoreParameters.previewVideoWidth);
                GLES20.glReadPixels(0, 0, resCoreParameters.previewVideoHeight, resCoreParameters.previewVideoWidth, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, pixBuffer);
                int[] glPixel = pixBuffer.array();
                int[] argbPixel = new int[resCoreParameters.previewVideoHeight * resCoreParameters.previewVideoWidth];
                ColorHelper.FIXGLPIXEL(glPixel, argbPixel, resCoreParameters.previewVideoHeight, resCoreParameters.previewVideoWidth);
                result = Bitmap.createBitmap(argbPixel,
                        resCoreParameters.previewVideoHeight,
                        resCoreParameters.previewVideoWidth,
                        Bitmap.Config.ARGB_8888);

                if (isEnableMirror && isEnablePreviewMirror) {
                    Matrix mx = new Matrix();
                    mx.setScale(-1, 1);  // 产生镜像
                    result = Bitmap.createBitmap(result, 0, 0, result.getWidth(), result.getHeight(), mx, true);
                }
            } catch (Exception e) {
                LogTools.trace("takescreenshot failed:", e);
            } finally {
  CallbackDelivery.i().post(new RESScreenShotListener.RESScreenShotListenerRunable(resScreenShotListener, result));
  resScreenShotListener = null;
}
}
}
}

private void initOffScreenGL() {
  if (offScreenGLWapper == null) {
        offScreenGLWapper = new OffScreenGLWapper();
        GLHelper.initOffScreenGL(offScreenGLWapper);
        GLHelper.makeCurrent(offScreenGLWapper);
        // camera
        offScreenGLWapper.camProgram = GLHelper.createCameraProgram();
        GLES20.glUseProgram(offScreenGLWapper.camProgram);
        offScreenGLWapper.camTextureLoc = GLES20.glGetUniformLocation(offScreenGLWapper.camProgram, "uTexture");
        offScreenGLWapper.camPostionLoc = GLES20.glGetAttribLocation(offScreenGLWapper.camProgram, "aPosition");
        offScreenGLWapper.camTextureCoordLoc = GLES20.glGetAttribLocation(offScreenGLWapper.camProgram, "aTextureCoord");
        // camera2d
        offScreenGLWapper.cam2dProgram = GLHelper.createCamera2DProgram();
        GLES20.glUseProgram(offScreenGLWapper.cam2dProgram);
        offScreenGLWapper.cam2dTextureLoc = GLES20.glGetUniformLocation(offScreenGLWapper.cam2dProgram, "uTexture");
        offScreenGLWapper.cam2dPostionLoc = GLES20.glGetAttribLocation(offScreenGLWapper.cam2dProgram, "aPosition");
        offScreenGLWapper.cam2dTextureCoordLoc = GLES20.glGetAttribLocation(offScreenGLWapper.cam2dProgram, "aTextureCoord");
        offScreenGLWapper.cam2dTextureMatrix = GLES20.glGetUniformLocation(offScreenGLWapper.cam2dProgram, "uTextureMatrix");
        int[] fb = new int[1], fbt = new int[1];
        GLHelper.createCamFrameBuff(fb, fbt, resCoreParameters.previewVideoHeight, resCoreParameters.previewVideoWidth);
        // resCoreParameters.videoWidth, resCoreParameters.videoHeight
        sample2DFrameBuffer = fb[0];
        sample2DFrameBufferTexture = fbt[0];
        GLHelper.createCamFrameBuff(fb, fbt, resCoreParameters.previewVideoHeight, resCoreParameters.previewVideoWidth);
        // resCoreParameters.videoWidth, resCoreParameters.videoHeight
        frameBuffer = fb[0];
        frameBufferTexture = fbt[0];
    } else {
    throw new IllegalStateException("initOffScreenGL without uninitOffScreenGL");
  }
}

@SuppressLint("NewApi")
private void uninitOffScreenGL() {
  if (offScreenGLWapper != null) {
    GLHelper.makeCurrent(offScreenGLWapper);
    GLES20.glDeleteProgram(offScreenGLWapper.camProgram);
    GLES20.glDeleteProgram(offScreenGLWapper.cam2dProgram);
    int[] fbArray = {frameBuffer, sample2DFrameBuffer};
  int[] fbtArray = {frameBufferTexture, sample2DFrameBufferTexture};
GLES20.glDeleteFramebuffers(2, fbArray, 0);
GLES20.glDeleteTextures(2, fbtArray, 0);
EGL14.eglDestroySurface(offScreenGLWapper.eglDisplay, offScreenGLWapper.eglSurface);
EGL14.eglDestroyContext(offScreenGLWapper.eglDisplay, offScreenGLWapper.eglContext);
EGL14.eglTerminate(offScreenGLWapper.eglDisplay);
EGL14.eglMakeCurrent(offScreenGLWapper.eglDisplay, EGL14.EGL_NO_SURFACE, EGL14.EGL_NO_SURFACE, EGL14.EGL_NO_CONTEXT);
} else {
  throw new IllegalStateException("uninitOffScreenGL without initOffScreenGL");
}
}

private void initScreenGL(SurfaceTexture screenSurfaceTexture) {
  if (screenGLWapper == null) {
    screenTexture = screenSurfaceTexture;
    screenGLWapper = new ScreenGLWapper();
    GLHelper.initScreenGL(screenGLWapper, offScreenGLWapper.eglContext, screenSurfaceTexture);
    GLHelper.makeCurrent(screenGLWapper);
    screenGLWapper.drawProgram = GLHelper.createScreenProgram();
    GLES20.glUseProgram(screenGLWapper.drawProgram);
    screenGLWapper.drawTextureLoc = GLES20.glGetUniformLocation(screenGLWapper.drawProgram, "uTexture");
    screenGLWapper.drawPostionLoc = GLES20.glGetAttribLocation(screenGLWapper.drawProgram, "aPosition");
    screenGLWapper.drawTextureCoordLoc = GLES20.glGetAttribLocation(screenGLWapper.drawProgram, "aTextureCoord");
  } else {
    throw new IllegalStateException("initScreenGL without unInitScreenGL");
  }
}

@SuppressLint("NewApi")
private void uninitScreenGL() {
  if (screenGLWapper != null) {
    GLHelper.makeCurrent(screenGLWapper);
    GLES20.glDeleteProgram(screenGLWapper.drawProgram);
    EGL14.eglDestroySurface(screenGLWapper.eglDisplay, screenGLWapper.eglSurface);
    EGL14.eglDestroyContext(screenGLWapper.eglDisplay, screenGLWapper.eglContext);
    EGL14.eglTerminate(screenGLWapper.eglDisplay);
    EGL14.eglMakeCurrent(screenGLWapper.eglDisplay, EGL14.EGL_NO_SURFACE, EGL14.EGL_NO_SURFACE, EGL14.EGL_NO_CONTEXT);
    screenGLWapper = null;
  } else {
    throw new IllegalStateException("unInitScreenGL without initScreenGL");
  }
}

private void initMediaCodecGL(Surface mediacodecSurface) {
  if (mediaCodecGLWapper == null) {
    mediaCodecGLWapper = new MediaCodecGLWapper();
    GLHelper.initMediaCodecGL(mediaCodecGLWapper, offScreenGLWapper.eglContext, mediacodecSurface);
    GLHelper.makeCurrent(mediaCodecGLWapper);
    GLES20.glEnable(GLES11Ext.GL_TEXTURE_EXTERNAL_OES);
    mediaCodecGLWapper.drawProgram = GLHelper.createMediaCodecProgram();
    GLES20.glUseProgram(mediaCodecGLWapper.drawProgram);
    mediaCodecGLWapper.drawTextureLoc = GLES20.glGetUniformLocation(mediaCodecGLWapper.drawProgram, "uTexture");
    mediaCodecGLWapper.drawPostionLoc = GLES20.glGetAttribLocation(mediaCodecGLWapper.drawProgram, "aPosition");
    mediaCodecGLWapper.drawTextureCoordLoc = GLES20.glGetAttribLocation(mediaCodecGLWapper.drawProgram, "aTextureCoord");
  } else {
    throw new IllegalStateException("initMediaCodecGL without uninitMediaCodecGL");
  }
}

@SuppressLint("NewApi")
private void uninitMediaCodecGL() {
  if (mediaCodecGLWapper != null) {
    GLHelper.makeCurrent(mediaCodecGLWapper);
    GLES20.glDeleteProgram(mediaCodecGLWapper.drawProgram);
    EGL14.eglDestroySurface(mediaCodecGLWapper.eglDisplay, mediaCodecGLWapper.eglSurface);
    EGL14.eglDestroyContext(mediaCodecGLWapper.eglDisplay, mediaCodecGLWapper.eglContext);
    EGL14.eglTerminate(mediaCodecGLWapper.eglDisplay);
    EGL14.eglMakeCurrent(mediaCodecGLWapper.eglDisplay, EGL14.EGL_NO_SURFACE, EGL14.EGL_NO_SURFACE, EGL14.EGL_NO_CONTEXT);
    mediaCodecGLWapper = null;
  } else {
    throw new IllegalStateException("uninitMediaCodecGL without initMediaCodecGL");
  }
}

private void resetFrameBuff() {
  GLHelper.makeCurrent(offScreenGLWapper);
  int[] fbArray = {frameBuffer, sample2DFrameBuffer};
int[] fbtArray = {frameBufferTexture, sample2DFrameBufferTexture};
GLES20.glDeleteFramebuffers(2, fbArray, 0);
GLES20.glDeleteTextures(2, fbtArray, 0);
int[] fb = new int[2], fbt = new int[2];
GLHelper.createCamFrameBuff(fb, fbt, resCoreParameters.videoWidth, resCoreParameters.videoHeight);
sample2DFrameBuffer = fb[0];
sample2DFrameBufferTexture = fbt[0];
frameBuffer = fb[1];
frameBufferTexture = fbt[1];
}

private void initBuffer() {
  shapeVerticesBuffer = GLHelper.getShapeVerticesBuffer();
  mediaCodecTextureVerticesBuffer = GLHelper.getMediaCodecTextureVerticesBuffer();
  screenTextureVerticesBuffer = GLHelper.getScreenTextureVerticesBuffer();
  updateCameraIndex(currCamera);
  drawIndecesBuffer = GLHelper.getDrawIndecesBuffer();
  cameraTextureVerticesBuffer = GLHelper.getCameraTextureVerticesBuffer();
}

public void updateCameraIndex(int cameraIndex) {
  synchronized (syncCameraTextureVerticesBuffer) {
    currCamera = cameraIndex;
    if (currCamera == Camera.CameraInfo.CAMERA_FACING_FRONT) {
      directionFlag = resCoreParameters.frontCameraDirectionMode ^ RESConfig.DirectionMode.FLAG_DIRECTION_FLIP_HORIZONTAL;
    } else {
      directionFlag = resCoreParameters.backCameraDirectionMode;
    }
    camera2dTextureVerticesBuffer = GLHelper.getCamera2DTextureVerticesBuffer(directionFlag, resCoreParameters.cropRatio);
  }
}

public float getDrawFrameRate() {
  return drawFrameRateMeter.getFps();
}

public void updateCamTexture(SurfaceTexture surfaceTexture) {
  synchronized (syncCameraTex) {
    if (surfaceTexture != cameraTexture) {
      cameraTexture = surfaceTexture;
      frameNum = 0;
      dropNextFrame = true;
    }
  }
}

public void addFrameNum() {
  synchronized (syncFrameNum) {
    ++frameNum;
    this.removeMessages(WHAT_FRAME);
    this.sendMessageAtFrontOfQueue(this.obtainMessage(VideoGLHandler.WHAT_FRAME));
  }
}

public void updatePreview(int w, int h) {
  screenSize = new Size(w, h);
}

public int getBufferTexture() {
  return frameBufferTexture;
}

@SuppressLint("NewApi")
private void encoderMp4(int bufferTexture) {
  synchronized (this) {
    if (mVideoEncoder != null) {
      processStMatrix(textureMatrix, mCameraId == Camera.CameraInfo.CAMERA_FACING_FRONT);
      if (mNeedResetEglContext) {
        mVideoEncoder.setEglContext(EGL14.eglGetCurrentContext(), bufferTexture);
        mNeedResetEglContext = false;
      }
      mVideoEncoder.setPreviewWH(resCoreParameters.previewVideoHeight, resCoreParameters.previewVideoWidth);
      mVideoEncoder.frameAvailableSoon(textureMatrix, mVideoEncoder.getMvpMatrix());
    }
  }
}



private MediaVideoEncoder mVideoEncoder;
private boolean mNeedResetEglContext = true;
private int mCameraId = -1;

@SuppressLint("NewApi")
public void setVideoEncoder(final MediaVideoEncoder encoder) {
  synchronized (this) {
    if (encoder != null) {
      encoder.setEglContext(EGL14.eglGetCurrentContext(), videoGLHander.getBufferTexture());
    }
    mVideoEncoder = encoder;
  }
}

private void processStMatrix(float[] matrix, boolean needMirror) {
    if (needMirror && matrix != null && matrix.length == 16) {
        for (int i = 0; i < 3; i++) {
            matrix[4 * i] = -matrix[4 * i];
        }

        if (matrix[4 * 3] == 0) {
            matrix[4 * 3] = 1.0f;
        } else if (matrix[4 * 3] == 1.0f) {
            matrix[4 * 3] = 0f;
        }
}
}

public void setNeedResetEglContext(boolean bol) {
  mNeedResetEglContext = bol;
}

}
