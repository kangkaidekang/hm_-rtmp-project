class RESSoftVideoCore implements RESVideoCore {
  private resCoreParameters: RESCoreParameters;
  private syncOp: object = {};
  private cameraTexture: SurfaceTexture | null;

  private currentCamera: number;
  private dstVideoEncoder: MediaCodec | null;
  private isEncoderStarted: boolean;
  private syncDstVideoEncoder: object = {};
  private dstVideoFormat: MediaFormat | null;
  //render
  private syncPreview: object = {};
  private previewRender: IRender | null;
  //filter
  private lockVideoFilter: Lock | null = null;
  private videoFilter: BaseSoftVideoFilter | null;
  private videoFilterHandler: VideoFilterHandler | null;
  private videoHandlerThread: HandlerThread | null;
  //sender
  private videoSenderThread: VideoSenderThread | null;
  //VideoBuffs
  //buffers to handle buff from queueVideo
  private orignVideoBuffs: RESVideoBuff[] | null;
  private lastVideoQueueBuffIndex: number;
  //buffer to convert orignVideoBuff to NV21 if filter are set
  private orignNV21VideoBuff: RESVideoBuff | null;
  //buffer to handle filtered color from filter if filter are set
  private filteredNV21VideoBuff: RESVideoBuff | null;
  //buffer to convert other color format to suitable color format for dstVideoEncoder if nessesary
  private suitable4VideoEncoderBuff: RESVideoBuff | null;

  private syncResScreenShotListener: object = {};
  private resScreenShotListener: RESScreenShotListener | null;

  private syncIsLooping: object = {};
  private isPreviewing: boolean = false;
  private isStreaming: boolean = false;
  private loopingInterval: number;

  constructor(parameters: RESCoreParameters) {
    this.resCoreParameters = parameters;
    this.lockVideoFilter = new ReentrantLock(false);
    this.videoFilter = null;
  }

  public setCurrentCamera(camIndex: number): void {
    if (this.currentCamera !== camIndex) {
      synchronized (this.syncOp) {
        if (this.videoFilterHandler !== null) {
          this.videoFilterHandler.removeMessages(VideoFilterHandler.WHAT_INCOMING_BUFF);
        }
        if (this.orignVideoBuffs !== null) {
          for (const buff of this.orignVideoBuffs) {
            buff.isReadyToFill = true;
          }
          this.lastVideoQueueBuffIndex = 0;
        }
      }
    }
    this.currentCamera = camIndex;
  }

  public prepare(resConfig: RESConfig): boolean {
    synchronized (this.syncOp) {
      this.resCoreParameters.renderingMode = resConfig.getRenderingMode();
      this.resCoreParameters.mediacdoecAVCBitRate = resConfig.getBitRate();
      this.resCoreParameters.videoBufferQueueNum = resConfig.getVideoBufferQueueNum();
      this.resCoreParameters.mediacodecAVCIFrameInterval = resConfig.getVideoGOP();
      this.resCoreParameters.mediacodecAVCFrameRate = this.resCoreParameters.videoFPS;
      this.loopingInterval = 1000 / this.resCoreParameters.videoFPS;
      this.dstVideoFormat = new MediaFormat();
      synchronized (this.syncDstVideoEncoder) {
        this.dstVideoEncoder = MediaCodecHelper.createSoftVideoMediaCodec(this.resCoreParameters, this.dstVideoFormat);
        this.isEncoderStarted = false;
        if (this.dstVideoEncoder == null) {
          LogTools.e("create Video MediaCodec failed");
          return false;
        }
      }
      this.resCoreParameters.previewBufferSize = BuffSizeCalculator.calculator(this.resCoreParameters.videoWidth,
        this.resCoreParameters.videoHeight, this.resCoreParameters.previewColorFormat);
      //video
      const videoWidth: number = this.resCoreParameters.videoWidth;
      const videoHeight: number = this.resCoreParameters.videoHeight;
      const videoQueueNum: number = this.resCoreParameters.videoBufferQueueNum;
      this.orignVideoBuffs = new Array(videoQueueNum).fill(null).map(() =>
      new RESVideoBuff(this.resCoreParameters.previewColorFormat, this.resCoreParameters.previewBufferSize)
      );
      this.lastVideoQueueBuffIndex = 0;
      this.orignNV21VideoBuff = new RESVideoBuff(MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420SemiPlanar,
        BuffSizeCalculator.calculator(videoWidth, videoHeight, MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420SemiPlanar));
      this.filteredNV21VideoBuff = new RESVideoBuff(MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420SemiPlanar,
        BuffSizeCalculator.calculator(videoWidth, videoHeight, MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420SemiPlanar));
      this.suitable4VideoEncoderBuff = new RESVideoBuff(this.resCoreParameters.mediacodecAVCColorFormat,
        BuffSizeCalculator.calculator(videoWidth, videoHeight, this.resCoreParameters.mediacodecAVCColorFormat));
      this.videoHandlerThread = new HandlerThread("videoFilterHandlerThread");
      this.videoHandlerThread.start();
      this.videoFilterHandler = new VideoFilterHandler(this.videoHandlerThread.getLooper());
      return true;
    }
  }

  public startStreaming(flvDataCollecter: RESFlvDataCollecter): boolean {
    synchronized (this.syncOp) {
      try {
        synchronized (this.syncDstVideoEncoder) {
          if (this.dstVideoEncoder == null) {
            this.dstVideoEncoder = MediaCodec.createEncoderByType(this.dstVideoFormat.getString(MediaFormat.KEY_MIME));
          }
          this.dstVideoEncoder.configure(this.dstVideoFormat, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
          this.dstVideoEncoder.start();
          this.isEncoderStarted = true;
        }
        this.videoSenderThread = new VideoSenderThread("VideoSenderThread", this.dstVideoEncoder, flvDataCollecter);
        this.videoSenderThread.start();
        synchronized (this.syncIsLooping) {
          if (!this.isPreviewing && !this.isStreaming) {
            this.videoFilterHandler.removeMessages(VideoFilterHandler.WHAT_DRAW);
            this.videoFilterHandler.sendMessageDelayed(this.videoFilterHandler.obtainMessage(VideoFilterHandler.WHAT_DRAW, SystemClock.uptimeMillis() + this.loopingInterval), this.loopingInterval);
          }
          this.isStreaming = true;
        }
      } catch (e) {
        LogTools.trace("RESVideoClient.start()failed", e);
        return false;
      }
      return true;
    }
  }

  public updateCamTexture(camTex: SurfaceTexture): void {
  }

  public stopStreaming(): boolean {
    synchronized (this.syncOp) {
      this.videoSenderThread.quit();
      synchronized (this.syncIsLooping) {
        this.isStreaming = false;
      }
      try {
        this.videoSenderThread.join();
      } catch (e) {
        LogTools.trace("RESCore", e);
      }
      synchronized (this.syncDstVideoEncoder) {
        this.dstVideoEncoder.stop();
        this.dstVideoEncoder.release();
        this.dstVideoEncoder = null;
        this.isEncoderStarted = false;
      }
      this.videoSenderThread = null;
      return true;
    }
  }


  public destroy(): boolean {
    synchronized (this.syncOp) {
      this.lockVideoFilter.lock();
      if (this.videoFilter !== null) {
        this.videoFilter.onDestroy();
      }
      this.lockVideoFilter.unlock();
      return true;
    }
  }

  @TargetApi(Build.VERSION_CODES.KITKAT)
  public reSetVideoBitrate(bitrate: number): void {
    synchronized (this.syncOp) {
      if (this.videoFilterHandler !== null) {
        this.videoFilterHandler.sendMessage(this.videoFilterHandler.obtainMessage(VideoFilterHandler.WHAT_RESET_BITRATE, bitrate, 0));
        this.resCoreParameters.mediacdoecAVCBitRate = bitrate;
        this.dstVideoFormat.setInteger(MediaFormat.KEY_BIT_RATE, this.resCoreParameters.mediacdoecAVCBitRate);
      }
    }
  }

  @TargetApi(Build.VERSION_CODES.KITKAT)
  public getVideoBitrate(): number {
    synchronized (this.syncOp) {
      return this.resCoreParameters.mediacdoecAVCBitRate;
    }
  }

  public reSetVideoFPS(fps: number): void {
    synchronized (this.syncOp) {
      this.resCoreParameters.videoFPS = fps;
      this.loopingInterval = 1000 / this.resCoreParameters.videoFPS;
    }
  }

  public reSetVideoSize(newParameters: RESCoreParameters): void {

  }

  public startPreview(surfaceTexture: SurfaceTexture, visualWidth: number, visualHeight: number): void {
    synchronized (this.syncPreview) {
      if (this.previewRender !== null) {
        throw new Error("startPreview without destroy previous");
      }
      switch (this.resCoreParameters.renderingMode) {
        case RESCoreParameters.RENDERING_MODE_NATIVE_WINDOW:
          this.previewRender = new NativeRender();
          break;
        case RESCoreParameters.RENDERING_MODE_OPENGLES:
          this.previewRender = new GLESRender();
          break;
        default:
          throw new Error("Unknow rendering mode");
      }
      this.previewRender.create(surfaceTexture,
        this.resCoreParameters.previewColorFormat,
        this.resCoreParameters.videoWidth,
        this.resCoreParameters.videoHeight,
        visualWidth,
        visualHeight);
      synchronized (this.syncIsLooping) {
        if (!this.isPreviewing && !this.isStreaming) {
          this.videoFilterHandler.removeMessages(VideoFilterHandler.WHAT_DRAW);
          this.videoFilterHandler.sendMessageDelayed(this.videoFilterHandler.obtainMessage(VideoFilterHandler.WHAT_DRAW, SystemClock.uptimeMillis() + this.loopingInterval), this.loopingInterval);
        }
        this.isPreviewing = true;
      }
    }
  }

  public updatePreview(visualWidth: number, visualHeight: number): void {
    synchronized (this.syncPreview) {
      if (this.previewRender === null) {
        throw new Error("updatePreview without startPreview");
      }
      this.previewRender.update(visualWidth, visualHeight);
    }
  }

  public stopPreview(releaseTexture: boolean): void {
    synchronized (this.syncPreview) {
      if (this.previewRender === null) {
        throw new Error("stopPreview without startPreview");
      }
      this.previewRender.destroy(releaseTexture);
      this.previewRender = null;
      synchronized (this.syncIsLooping) {
        this.isPreviewing = false;
      }
    }
  }

  public queueVideo(rawVideoFrame: Uint8Array): void {
    synchronized (this.syncOp) {
      const targetIndex: number = (this.lastVideoQueueBuffIndex + 1) % this.orignVideoBuffs.length;
      if (this.orignVideoBuffs[targetIndex].isReadyToFill) {
        LogTools.d("queueVideo,accept ,targetIndex" + targetIndex);
        this.acceptVideo(rawVideoFrame, this.orignVideoBuffs[targetIndex].buff);
        this.orignVideoBuffs[targetIndex].isReadyToFill = false;
        this.lastVideoQueueBuffIndex = targetIndex;
      } else {
        LogTools.d("queueVideo,reject ,targetIndex" + targetIndex);
      }
    }
  }
  private acceptVideo(src: Uint8Array, dst: Uint8Array): void {
    const directionFlag = currentCamera === Camera.CameraInfo.CAMERA_FACING_BACK ? resCoreParameters.backCameraDirectionMode : resCoreParameters.frontCameraDirectionMode;
    ColorHelper.NV21Transform(src,
      dst,
      resCoreParameters.previewVideoWidth,
      resCoreParameters.previewVideoHeight,
      directionFlag);
  }

  acquireVideoFilter(): BaseSoftVideoFilter {
    lockVideoFilter.lock();
    const filter = videoFilter;
    lockVideoFilter.unlock();
    return filter;
  }

  releaseVideoFilter(): void {
    lockVideoFilter.unlock();
  }

  setVideoFilter(baseSoftVideoFilter: BaseSoftVideoFilter): void {
    lockVideoFilter.lock();
    if (videoFilter !== null) {
      videoFilter.onDestroy();
    }
    videoFilter = baseSoftVideoFilter;
    if (videoFilter !== null) {
      videoFilter.onInit(resCoreParameters.videoWidth, resCoreParameters.videoHeight);
    }
    lockVideoFilter.unlock();
  }

  takeScreenShot(listener: RESScreenShotListener): void {
    synchronized (syncResScreenShotListener) {
      resScreenShotListener = listener;
    }
  }

  setVideoChangeListener(listener: RESVideoChangeListener): void {
  }

  getDrawFrameRate(): number {
    synchronized (syncOp) {
      return videoFilterHandler === null ? 0 : videoFilterHandler.getDrawFrameRate();
    }
  }

  private class VideoFilterHandler {
  static FILTER_LOCK_TOLERATION: number = 3; // 3ms
  static WHAT_INCOMING_BUFF: number = 1;
  static WHAT_DRAW: number = 2;
  static WHAT_RESET_BITRATE: number = 3;
  sequenceNum: number;
  drawFrameRateMeter: RESFrameRateMeter;

  constructor(looper: Looper) {
    super(looper);
    this.sequenceNum = 0;
    this.drawFrameRateMeter = new RESFrameRateMeter();
  }

  getDrawFrameRate(): number {
    return this.drawFrameRateMeter.getFps();
  }

  handleMessage(msg: Message): void {
    switch (msg.what) {
      case VideoFilterHandler.WHAT_INCOMING_BUFF: {
        const targetIndex: number = msg.arg1;
        /**
         * orignVideoBuffs[targetIndex] is ready
         * orignVideoBuffs[targetIndex]->orignNV21VideoBuff
         */
        dst.set(orignVideoBuffs[targetIndex].buff);
        orignVideoBuffs[targetIndex].isReadyToFill = true;
      }
        break;
      case VideoFilterHandler.WHAT_DRAW: {
        const time: number = msg.obj;
        const interval: number = time + loopingInterval - SystemClock.uptimeMillis();
        synchronized (syncIsLooping) {
          if (isPreviewing || isStreaming) {
            if (interval > 0) {
              videoFilterHandler.sendMessageDelayed(videoFilterHandler.obtainMessage(
                VideoFilterHandler.WHAT_DRAW,
                SystemClock.uptimeMillis() + interval),
                interval);
            } else {
              videoFilterHandler.sendMessage(videoFilterHandler.obtainMessage(
                VideoFilterHandler.WHAT_DRAW,
                SystemClock.uptimeMillis() + loopingInterval));
            }
          }
        }
        this.sequenceNum++;
        const nowTimeMs: number = SystemClock.uptimeMillis();
        const isFilterLocked: boolean = this.lockVideoFilter();
        if (isFilterLocked) {
          let modified: boolean;
          modified = videoFilter.onFrame(orignNV21VideoBuff.buff, filteredNV21VideoBuff.buff, nowTimeMs, this.sequenceNum);
          this.unlockVideoFilter();
          this.rendering(modified ? filteredNV21VideoBuff.buff : orignNV21VideoBuff.buff);
          this.checkScreenShot(modified ? filteredNV21VideoBuff.buff : orignNV21VideoBuff.buff);
          /**
           * orignNV21VideoBuff is ready
           * orignNV21VideoBuff->suitable4VideoEncoderBuff
           */
          if (resCoreParameters.mediacodecAVCColorFormat === MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420SemiPlanar) {
            ColorHelper.NV21TOYUV420SP(modified ? filteredNV21VideoBuff.buff : orignNV21VideoBuff.buff,
              suitable4VideoEncoderBuff.buff, resCoreParameters.videoWidth * resCoreParameters.videoHeight);
          } else if (resCoreParameters.mediacodecAVCColorFormat === MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420Planar) {
            ColorHelper.NV21TOYUV420P(modified ? filteredNV21VideoBuff.buff : orignNV21VideoBuff.buff,
              suitable4VideoEncoderBuff.buff, resCoreParameters.videoWidth * resCoreParameters.videoHeight);
          } else {// LAKETODO colorConvert
          }
        } else {
          this.rendering(orignNV21VideoBuff.buff);
          this.checkScreenShot(orignNV21VideoBuff.buff);
          if (resCoreParameters.mediacodecAVCColorFormat === MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420SemiPlanar) {
            ColorHelper.NV21TOYUV420SP(orignNV21VideoBuff.buff,
              suitable4VideoEncoderBuff.buff,
              resCoreParameters.videoWidth * resCoreParameters.videoHeight);
          } else if (resCoreParameters.mediacodecAVCColorFormat === MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420Planar) {
            ColorHelper.NV21TOYUV420P(orignNV21VideoBuff.buff,
              suitable4VideoEncoderBuff.buff,
              resCoreParameters.videoWidth * resCoreParameters.videoHeight);
          }
          orignNV21VideoBuff.isReadyToFill = true;
        }
        this.drawFrameRateMeter.count();
        // suitable4VideoEncoderBuff is ready
        synchronized (syncDstVideoEncoder) {
          if (dstVideoEncoder !== null && isEncoderStarted) {
            const eibIndex: number = dstVideoEncoder.dequeueInputBuffer(-1);
            if (eibIndex >= 0) {
              const dstVideoEncoderIBuffer: ByteBuffer = dstVideoEncoder.getInputBuffers()[eibIndex];
              dstVideoEncoderIBuffer.position(0);
              dstVideoEncoderIBuffer.put(suitable4VideoEncoderBuff.buff, 0, suitable4VideoEncoderBuff.buff.length);
              dstVideoEncoder.queueInputBuffer(eibIndex, 0, suitable4VideoEncoderBuff.buff.length, nowTimeMs * 1000, 0);
            } else {
              LogTools.d("dstVideoEncoder.dequeueInputBuffer(-1)<0");
            }
          }
        }

        LogTools.d("VideoFilterHandler,ProcessTime:" + (System.currentTimeMillis() - nowTimeMs));
      }
        break;
      case VideoFilterHandler.WHAT_RESET_BITRATE: {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && dstVideoEncoder !== null) {
          const bitrateBundle: Bundle = new Bundle();
          bitrateBundle.putInt(MediaCodec.PARAMETER_KEY_VIDEO_BITRATE, msg.arg1);
          dstVideoEncoder.setParameters(bitrateBundle);
        }
      }
        break;
    }
  }

  /**
   * rendering nv21 using native window
   *
   * @param pixel
   */
  private rendering(pixel: Uint8Array): void {
    synchronized (syncPreview) {
      if (previewRender === null) {
        return;
      }
      previewRender.rendering(pixel);
    }
  }

  /**
   * check if screenshotlistener exist
   *
   * @param pixel
   */
  private checkScreenShot(pixel: Uint8Array): void {
    synchronized (syncResScreenShotListener) {
      if (resScreenShotListener !== null) {
        const argbPixel: number[] = new Array(resCoreParameters.videoWidth * resCoreParameters.videoHeight);
        ColorHelper.NV21TOARGB(pixel,
          argbPixel,
          resCoreParameters.videoWidth,
          resCoreParameters.videoHeight);
        const result: Bitmap = Bitmap.createBitmap(argbPixel,
          resCoreParameters.videoWidth,
          resCoreParameters.videoHeight,
          Bitmap.Config.ARGB_8888);
        CallbackDelivery.i().post(new RESScreenShotListener.RESScreenShotListenerRunable(resScreenShotListener, result));
        resScreenShotListener = null;
      }
    }
  }

  /**
   * @return ture if filter locked & filter!=null
   */

  private lockVideoFilter(): boolean {
    try {
      const locked: boolean = lockVideoFilter.tryLock(FILTER_LOCK_TOLERATION, TimeUnit.MILLISECONDS);
      if (locked) {
        if (videoFilter !== null) {
          return true;
        } else {
          lockVideoFilter.unlock();
          return false;
        }
      } else {
        return false;
      }
    } catch (e) {
    }
    return false;
  }

  private unlockVideoFilter(): void {
    lockVideoFilter.unlock();
  }


setVideoEncoder(encoder: MediaVideoEncoder): void {
}

setMirror(isEnableMirror: boolean, isEnablePreviewMirror: boolean, isEnableStreamMirror: boolean): void {
}

setNeedResetEglContext(bol: boolean): void {
}
}