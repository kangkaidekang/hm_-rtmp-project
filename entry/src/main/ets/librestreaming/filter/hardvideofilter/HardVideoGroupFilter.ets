import { BaseHardVideoFilter } from './BaseHardVideoFilter';
import { GLESTools } from '../tools/GLESTools';
import { FloatBuffer } from 'java.nio';

export class HardVideoGroupFilter extends BaseHardVideoFilter {
  private filterWrappers: FilterWrapper[];

  constructor(filters: BaseHardVideoFilter[]) {
    super();
    if (filters == null || filters.length === 0) {
      throw new Error("Cannot create empty GroupFilter");
    }
    this.filterWrappers = filters.map(filter => new FilterWrapper(filter));
  }

  public onInit(VWidth: number, VHeight: number): void {
    super.onInit(VWidth, VHeight);
    this.filterWrappers.forEach(wrapper => {
      wrapper.filter.onInit(VWidth, VHeight);
      const frameBuffer = new Int32Array(1);
      const frameBufferTexture = new Int32Array(1);
      GLESTools.createFrameBuff(frameBuffer, frameBufferTexture, this.SIZE_WIDTH, this.SIZE_HEIGHT);
      wrapper.frameBuffer = frameBuffer[0];
      wrapper.frameBufferTexture = frameBufferTexture[0];
    });
  }

  public onDraw(cameraTexture: number, targetFrameBuffer: number, shapeBuffer: FloatBuffer, textrueBuffer: FloatBuffer): void {
    let preFilterWrapper: FilterWrapper | null = null;
    let texture: number;
    this.filterWrappers.forEach((wrapper, i) => {
      if (preFilterWrapper === null) {
        texture = cameraTexture;
      } else {
        texture = preFilterWrapper.frameBufferTexture;
      }
      if (i === this.filterWrappers.length - 1) {
        wrapper.filter.onDraw(texture, targetFrameBuffer, shapeBuffer, textrueBuffer);
      } else {
        wrapper.filter.onDraw(texture, wrapper.frameBuffer, shapeBuffer, textrueBuffer);
      }
      preFilterWrapper = wrapper;
    });
  }

  public onDestroy(): void {
    super.onDestroy();
    this.filterWrappers.forEach(wrapper => {
      wrapper.filter.onDestroy();
      glDeleteFramebuffers(1, [wrapper.frameBuffer], 0);
      glDeleteTextures(1, [wrapper.frameBufferTexture], 0);
    });
  }

  public onDirectionUpdate(_directionFlag: number): void {
    super.onDirectionUpdate(_directionFlag);
    this.filterWrappers.forEach(wrapper => {
      wrapper.filter.onDirectionUpdate(_directionFlag);
    });
  }
}

class FilterWrapper {
  filter: BaseHardVideoFilter;
  frameBuffer: number;
  frameBufferTexture: number;

  constructor(filter: BaseHardVideoFilter) {
    this.filter = filter;
  }
}
