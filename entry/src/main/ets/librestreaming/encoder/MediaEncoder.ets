import { MediaCodec, MediaFormat } from 'android.media';
import { MediaMuxerWrapper } from './MediaMuxerWrapper'; // Assuming MediaMuxerWrapper is defined in a separate file

abstract class MediaEncoder implements Runnable {

  private static readonly DEBUG: boolean = false; // TODO set false on release
  private static readonly TAG: string = "MediaEncoder";

  protected static readonly TIMEOUT_USEC: number = 10000; // 10[msec]
  protected static readonly MSG_FRAME_AVAILABLE: number = 1;
  protected static readonly MSG_STOP_RECORDING: number = 9;

  interface MediaEncoderListener {
    onPrepared(encoder: MediaEncoder): void;
    onStopped(encoder: MediaEncoder): void;
  }

  protected mSync: Object = {};
  protected mIsCapturing: boolean = false;
  private mRequestDrain: number = 0;
  protected mRequestStop: boolean = false;
  protected mIsEOS: boolean = false;
  protected mMuxerStarted: boolean = false;
  protected mTrackIndex: number = 0;
  protected mMediaCodec: MediaCodec | null = null;
  protected readonly mWeakMuxer: WeakReference<MediaMuxerWrapper>;
  private mBufferInfo: MediaCodec.BufferInfo | null = null;
  protected readonly mListener: MediaEncoderListener;

public constructor(muxer: MediaMuxerWrapper, listener: MediaEncoderListener) {
  if (listener === null) throw new Error("MediaEncoderListener is null");
  if (muxer === null) throw new Error("MediaMuxerWrapper is null");
  this.mWeakMuxer = new WeakRef(muxer);
  muxer.addEncoder(this);
  this.mListener = listener;
  synchronized(this.mSync) {
    // 在这里创建 BufferInfo，以提高效率（减少 GC）
    this.mBufferInfo = new MediaCodec.BufferInfo();
    // 等待线程启动
    new Thread(this, this.constructor.name).start();
    try {
      this.mSync.wait();
    } catch (e) {
    }
  }
}

public getOutputPath(): string | null {
  const muxer: MediaMuxerWrapper | null = this.mWeakMuxer.get();
  return muxer !== null ? muxer.getOutputPath() : null;
}

/**
 * 表示帧数据很快就会可用或已经可用的方法
 * @return 如果编码器准备好编码，则返回 true
 */
public frameAvailableSoon(): boolean {
  if (DEBUG) console.log("frameAvailableSoon");
  synchronized(this.mSync) {
    if (!this.mIsCapturing || this.mRequestStop) {
      return false;
    }
    this.mRequestDrain++;
    this.mSync.notifyAll();
  }
  return true;
}

/**
 * 私有线程上的编码循环
 */
public run(): void {
  synchronized(this.mSync) {
  this.mRequestStop = false;
  this.mRequestDrain = 0;
  this.mSync.notify();
}
let isRunning: boolean = true;
let localRequestStop: boolean;
let localRequestDrain: boolean;
while (isRunning) {
  synchronized(this.mSync) {
    localRequestStop = this.mRequestStop;
    localRequestDrain = (this.mRequestDrain > 0);
    if (localRequestDrain) this.mRequestDrain--;
  }
  if (localRequestStop) {
    this.drain();
    // 请求停止录制
    this.signalEndOfInputStream();
    // 为了处理 EOS 信号，再次处理输出数据
    this.drain();
    // 释放所有相关对象
    this.release();
    break;
  }
  if (localRequestDrain) {
    this.drain();
  } else {
    synchronized(this.mSync) {
      try {
        this.mSync.wait();
      } catch (e) {
        break;
      }
    }
  }
}
if (DEBUG) console.log("Encoder thread exiting");
synchronized(this.mSync) {
  this.mRequestStop = true;
  this.mIsCapturing = false;
}
}

/**
 * 为每个子类准备的准备方法
 * 这个方法应该在子类中实现，因此将其设置为抽象方法
 * @throws IOException
 */
protected abstract prepare(): void;

protected startRecording(): void {
  if (DEBUG) console.log("startRecording");
  synchronized(this.mSync) {
  this.mIsCapturing = true;
  this.mRequestStop = false;
  this.mSync.notifyAll();
}
}

/**
 * 请求停止编码的方法
 */
protected stopRecording(): void {
  if (DEBUG) console.log("stopRecording");
  synchronized(this.mSync) {
  if (!this.mIsCapturing || this.mRequestStop) return;
  this.mRequestStop = true; // 用于拒绝更新的帧
  this.mSync.notifyAll();
  // 我们无法知道编码和写入何时完成。
  // 因此，我们立即返回请求，以避免调用线程的延迟
}
}

//********************************************************************************
//********************************************************************************

/**
 * 释放所有相关对象
 */
protected release(): void {
  if (DEBUG) console.log("release:");
  try {
    this.mListener.onStopped(this);
  } catch (e) {
    console.error("failed onStopped", e);
  }
  this.mIsCapturing = false;
  if (this.mMediaCodec !== null) {
    try {
      this.mMediaCodec.stop();
      this.mMediaCodec.release();
      this.mMediaCodec = null;
    } catch (e) {
      console.error("failed releasing MediaCodec", e);
    }
  }
  if (this.mMuxerStarted) {
    const muxer: MediaMuxerWrapper | null = this.mWeakMuxer !== null ? this.mWeakMuxer.get() : null;
    if (muxer !== null) {
      try {
        muxer.stop();
      } catch (e) {
        console.error("failed stopping muxer", e);
      }
    }
  }
  this.mBufferInfo = null;
}

protected signalEndOfInputStream(): void {
  if (DEBUG) console.log("sending EOS to encoder");
  // signalEndOfInputStream 仅适用于带有 surface 的视频编码
  // 等效地发送一个带有 BUFFER_FLAG_END_OF_STREAM 标志的空缓冲区。
  // mMediaCodec.signalEndOfInputStream();	// API >= 18
  this.encode(null, 0, this.getPTSUs());
}

/**
 * 将字节数组设置为 MediaCodec 编码器的方法
 * @param buffer
 * @param length 字节数组的长度，零表示 EOS。
 * @param presentationTimeUs
 */
protected encode(buffer: ByteBuffer | null, length: number, presentationTimeUs: number): void {
  if (!this.mIsCapturing) return;
  const inputBuffers: ByteBuffer[] = this.mMediaCodec.getInputBuffers();
  while (this.mIsCapturing) {
  const inputBufferIndex: number = this.mMediaCodec.dequeueInputBuffer(TIMEOUT_USEC);
  if (inputBufferIndex >= 0) {
  const inputBuffer: ByteBuffer = inputBuffers[inputBufferIndex];
  inputBuffer.clear();
  if (buffer !== null) {
  inputBuffer.put(buffer);
}
if (DEBUG) console.log("encode:queueInputBuffer");
if (length <= 0) {
  // 发送 EOS
  this.mIsEOS = true;
  if (DEBUG) console.log("send BUFFER_FLAG_END_OF_STREAM");
  this.mMediaCodec.queueInputBuffer(inputBufferIndex, 0, 0, presentationTimeUs, MediaCodec.BUFFER_FLAG_END_OF_STREAM);
  break;
} else {
  this.mMediaCodec.queueInputBuffer(inputBufferIndex, 0, length, presentationTimeUs, 0);
}
break;
} else if (inputBufferIndex === MediaCodec.INFO_TRY_AGAIN_LATER) {
  // 等待 MediaCodec 编码器准备好编码
  // 这里没有什么要做的，因为 MediaCodec#dequeueInputBuffer(TIMEOUT_USEC)
  // 每次调用都会等待最长 TIMEOUT_USEC（10 毫秒）
}
}
}

/**
 * 释放编码数据并将其写入 muxer 的方法
 */
protected drain(): void {
  if (this.mMediaCodec === null) return;
  let encoderOutputBuffers: ByteBuffer[] = this.mMediaCodec.getOutputBuffers();
  let encoderStatus: number, count: number = 0;
  const muxer: MediaMuxerWrapper | null = this.mWeakMuxer.get();
  if (muxer === null) {
  // throw new Error("muxer is unexpectedly null");
  console.warn("muxer is unexpectedly null");
  return;
}
while (this.mIsCapturing) {
  // 获取最长 TIMEOUT_USEC（=10 毫秒）的编码数据
  encoderStatus = this.mMediaCodec.dequeueOutputBuffer(this.mBufferInfo, TIMEOUT_USEC);
  if (encoderStatus === MediaCodec.INFO_TRY_AGAIN_LATER) {
    // 等待 5 个计数（=TIMEOUT_USEC x 5 = 50 毫秒）直到数据/EOS 到来
    if (!this.mIsEOS) {
      if (++count > 5) break; // 跳出循环
    }
  } else if (encoderStatus === MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
    // 这不应该在编码时出现
    encoderOutputBuffers = this.mMediaCodec.getOutputBuffers();
  } else if (encoderStatus === MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
    // 这个状态表示编解码器的输出格式已更改
    // 这应该在实际编码数据之前只出现一次
    // 但在 Android 4.3 或更低版本中，这种状态从不出现
    // 在这种情况下，您应该在 MediaCodec.BUFFER_FLAG_CODEC_CONFIG 来临时处理。
    if (this.mMuxerStarted) {
      // 第二次请求是错误的
      throw new Error("format changed twice");
    }
    // 从编解码器中获取输出格式并将其传递给 muxer
    // INFO_OUTPUT_FORMAT_CHANGED 之后应该调用 getOutputFormat，否则会崩溃。
    const format: MediaFormat = this.mMediaCodec.getOutputFormat(); // API >= 16
    this.mTrackIndex = muxer.addTrack(format);
    this.mMuxerStarted = true;
    if (!muxer.start()) {
      // 我们应该等到 muxer 准备就绪
      synchronized(muxer) {
        while (!muxer.isStarted()) {
          try {
            muxer.wait(100);
          } catch (e) {
            break;
          }
        }
      }
    }
  } else if (encoderStatus < 0) {
    // 意外状态
    if (DEBUG) console.warn("drain:unexpected result from encoder#dequeueOutputBuffer: " + encoderStatus);
  } else {
    const encodedData: ByteBuffer | null = encoderOutputBuffers[encoderStatus];
    if (encodedData === null) {
      // 这不应该发生...可能是 MediaCodec 的内部错误
      throw new Error("encoderOutputBuffer " + encoderStatus + " was null");
    }
    if ((this.mBufferInfo.flags & MediaCodec.BUFFER_FLAG_CODEC_CONFIG) !== 0) {
      // 在目标为 Android 4.3 或更低版本时，您应该在这里设置输出格式给 muxer
      // 但是在这里无法调用 MediaCodec#getOutputFormat（因为 INFO_OUTPUT_FORMAT_CHANGED 还没有到来）
      // 因此，我们应该从缓冲区数据中展开和准备输出格式。
      // 这个示例是为 API >= 18（>=Android 4.3）编写的，所以这里忽略这个标志
      if (DEBUG) console.log("drain:BUFFER_FLAG_CODEC_CONFIG");
      this.mBufferInfo.size = 0;
    }

    if (this.mBufferInfo.size !== 0) {
      // 编码数据已准备就绪，清除等待计数器
      count = 0;
      if (!this.mMuxerStarted) {
        // muxer 还没有准备好...这将导致编程失败。
        throw new Error("drain:muxer hasn't started");
      }
      // 将编码数据写入 muxer（需要调整 presentationTimeUs。
      this.mBufferInfo.presentationTimeUs = this.getPTSUs();
      muxer.writeSampleData(this.mTrackIndex, encodedData, this.mBufferInfo);
      this.prevOutputPTSUs = this.mBufferInfo.presentationTimeUs;
    }
    // 返回缓冲区给编码器
    this.mMediaCodec.releaseOutputBuffer(encoderStatus, false);
    if ((this.mBufferInfo.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) !== 0) {
      // 当 EOS 到来时。
      this.mIsCapturing = false;
      break; // 跳出循环
    }
  }
}
}

/**
 * 上一个写入的 presentationTimeUs
 */
private prevOutputPTSUs: number = 0;
/**
 * 获取下一个编码的 presentationTimeUs
 * @return
 */
protected getPTSUs(): number {
  let result: number = Date.now() * 1000;
  // presentationTimeUs 应该是单调递增的
  // 否则 muxer 会写入失败
  if (result < this.prevOutputPTSUs) {
    result = (this.prevOutputPTSUs - result) + result;
  }
  return result;
}

}