export const subtraction: (a: number, b: number) => number;
export const open: (url: string, isPublishMode: boolean) => number;
export const read: (rtmpPointer: number, data: Uint8Array,offset: number, size: number) => number;
export const write: (rtmpPointer: number, data: Uint8Array,size: number, type: number, ts: number) => number;
export const close: (rtmpPointer: number) => number;
export const getIpAddr: (rtmpPointer: number) => string;