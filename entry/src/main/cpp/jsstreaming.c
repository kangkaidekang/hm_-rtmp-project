//
// Created on 2024/4/18.
//
// Node APIs are not fully supported. To solve the compilation error of the interface cannot be found,
// please include "napi/native_api.h".

#include "napi/native_api.h"

static napi_value Subtraction(napi_env env, napi_callback_info info) {
    size_t argc = 2;
    napi_value args[2] = {NULL};

    napi_get_cb_info(env, info, &argc, args, NULL, NULL);

    napi_valuetype valuetype0;
    napi_typeof(env, args[0], &valuetype0);

    napi_valuetype valuetype1;
    napi_typeof(env, args[1], &valuetype1);

    double value0;
    napi_get_value_double(env, args[0], &value0);

    double value1;
    napi_get_value_double(env, args[1], &value1);

    napi_value sub;
    napi_create_double(env, value0 - value1, &sub);

    return sub;
}

static napi_value NV21TOYUV420SP(napi_env env, napi_callback_info info) {
    size_t argc = 3;
    napi_value args[3] = {NULL};

    napi_get_cb_info(env, info, &argc, args, NULL, NULL);

    napi_valuetype valuetype0;
    napi_typeof(env, args[0], &valuetype0);

    napi_valuetype valuetype1;
    napi_typeof(env, args[1], &valuetype1);

    napi_valuetype valuetype2;
    napi_typeof(env, args[2], &valuetype2);

    napi_status url_status;
    char url[512];
    size_t url_length;
    url_status = napi_get_value_string_utf8(env, args[0], url, sizeof(url), &url_length);
    if (url_status != napi_ok) {
        napi_throw(env, "Failed to get url value");
        return NULL;
    }
}

EXTERN_C_START
static napi_value Init(napi_env env, napi_value exports) {
    napi_property_descriptor desc[] = {{"subtraction", NULL, Subtraction, NULL, NULL, NULL, napi_default, NULL},
                                       {"NV21TOYUV420SP", NULL, NV21TOYUV420SP, NULL, NULL, NULL, napi_default, NULL}};
    napi_define_properties(env, exports, sizeof(desc) / sizeof(desc[0]), desc);
    return exports;
}
EXTERN_C_END

static napi_module demoModule = {
    .nm_version = 1,
    .nm_flags = 0,
    .nm_filename = NULL,
    .nm_register_func = Init,
    .nm_modname = "rertmp",
    .nm_priv = ((void *)0),
    .reserved = {0},
};

__attribute__((constructor)) void RegisterRertmpModule(void) { napi_module_register(&demoModule); }