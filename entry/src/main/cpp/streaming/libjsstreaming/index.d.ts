export const subtraction: (a: number, b: number) => number;
export const NV21TOYUV420SP: (src: Uint8Array, dst: Uint8Array, YSize: number) => void;
export const NV21TOYUV420P: (src: Uint8Array, dst: Uint8Array, YSize: number) => void;
export const YUV420SPTOYUV420P: (src: Uint8Array, dst: Uint8Array, YSize: number) => void;
export const NV21TOARGB: (src: Uint8Array, dst: number[], width: number, height: number) => void;
export const FIXGLPIXEL: (src: Uint8Array, dst: number[], width: number, height: number) => void;
export const NV21Transform: (src: Uint8Array, dst: number[], srcwidth: number, srcheight: number, directionFlag: number) => void;